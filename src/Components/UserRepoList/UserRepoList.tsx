import React, { FC } from 'react';
import { TReposData } from '../../types';
import './UserRepoList.css';

type TProps = {
  item: TReposData;
};

export const UserRepoList: FC<TProps> = ({ item }) => {
  return (
    <section className="repository-list__item" key={item.id}>
      <h3 className="repository-list__item-title">
        <a href={item.html_url} className="link">
          {item.name}
        </a>
      </h3>
      <p className="repository-list__item-text">{item.description}</p>
    </section>
  );
};
