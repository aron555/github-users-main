import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter as Router } from 'react-router-dom';
import './common.css';
import { App } from '@components/App/App';

const container = document.getElementById('root') as HTMLBaseElement;
const root = createRoot(container);

root.render(
  <Router>
    <App />
  </Router>
);
