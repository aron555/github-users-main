import React, { FC, useEffect, useState } from 'react';
import { getUserCompanyName, getUserReposLength } from '../../api';
import { Link } from 'react-router-dom';
import './UserInfo.css';

type TProps = {
  login: string;
  avatar_url: string;
};

export const UserInfo: FC<TProps> = ({ login, avatar_url }) => {
  const [reposCount, setReposCount] = useState<number>(0);
  const [company, setCompany] = useState<string>('');

  useEffect(() => {
    (async () => {
      setReposCount(await getUserReposLength(login));
      setCompany(await getUserCompanyName(login));
    })();
  }, [login]);

  return (
    <section className="users-list__item">
      <div className="users-list__image-container">
        <img className="users-list__image" src={avatar_url} alt={`${login} profile photo`} />
      </div>
      <div className="users-list__content">
        <h2 className="users-list__title">
          <Link to={`/users/${login}`} className="link">
            {login}
          </Link>
          , {reposCount} репозиториев
        </h2>
        <p className="users-list__text">{company}</p>
      </div>
    </section>
  );
};
