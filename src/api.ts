import { Octokit } from '@octokit/rest';
import { TSearchItems, TUserData } from './types';

const client = new Octokit({
  auth: process.env.GITHUB_TOKEN,
});

/* TODO: реализовать проверки и исключения */

export const getUsers = async (): Promise<TUserData[]> => {
  const resp = await client.request(`GET /users?since=0&per_page=9`, {});

  return resp.data;
};

export const getRepo = async (login: string): Promise<any[]> => {
  const resp = await client.request(`/users/${login}/repos`);

  return resp.data;
};

export const getUserReposLength = async (login: string): Promise<number> => {
  const data = await client.paginate(`GET /users/${login}/repos`, {});

  return data.length;
};

export const getUserCompanyName = async (login: string): Promise<string> => {
  const company = await client.request(`GET /users/${login}/orgs`, {});

  return company.data[0]?.login || '';
};

export const getUser = async (login: string | undefined): Promise<TUserData | null> => {
  if (login !== undefined) {
    try {
      const user = await client.request(`GET /users/${login}`, {});

      return user.data;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(`Не удалось получить пользователя ${login}`, error);
      throw error;
    }
  }

  return null;
};

export const searchUser = async (searchValue: string | undefined): Promise<TSearchItems | null> => {
  if (searchValue !== undefined) {
    try {
      const search = await client.request(`GET /search/users?q=${searchValue}&since=0&per_page=9`, {});

      return search.data.items;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error('search error', error);
      throw error;
    }
  }

  return null;
};
