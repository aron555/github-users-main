import React, { FC } from 'react';
import { Routes, Route, Outlet } from 'react-router-dom';
import { UserProfilePage } from '@components/UserProfilePage/UserProfilePage';
import { UsersList } from '@components/UsersList/UsersList';
import { UsersSearchPage } from '@components/UsersSearchPage/UsersSearchPage';
import { Header } from '@components/Header/Header';
import './App.css';

export const App: FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<UsersList />} />
        <Route path={`users/`} element={<UsersList />} />
        <Route path={`users/:id`} element={<UserProfilePage />} />
        <Route path={`search`} element={<UsersSearchPage />} />
        <Route path="*" element={<UsersList />} />
      </Route>
    </Routes>
  );
};

function Layout() {
  return (
    <>
      <Header />
      <main>
        <div className="container">
          <Outlet />
        </div>
      </main>
    </>
  );
}
