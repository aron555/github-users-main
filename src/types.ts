import { Endpoints } from '@octokit/types';

export type TUserData = Endpoints['GET /user']['response']['data'];

type listUserReposResponse = Endpoints['GET /repos/{owner}/{repo}']['response'];

export type TReposData = listUserReposResponse['data'];

export type TSearchItems = Endpoints['GET /search/users']['response']['data']['items'];
