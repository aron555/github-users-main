import React, { FC, useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { searchUser } from '../../api';
import { UserInfo } from '@components/UserInfo/UserInfo';
import { TSearchItems } from '../../types';
import './UsersSearchPage.css';

export const UsersSearchPage: FC = () => {
  const [searchParams] = useSearchParams();

  const [users, setUsers] = useState<TSearchItems>([]);

  const query = searchParams.get('query') || '';

  const navigator = useNavigate();

  useEffect(() => {
    searchUser(query).then((items) => {
      if (items !== null) {
        return setUsers(items);
      }

      navigator('/');
    });
  }, [query, users]);

  if (users.length === 0) {
    return <h1 className="title">Ничего не найдено по запросу {query}</h1>;
  }

  return (
    <>
      <h1 className="title">Пользователи по запросу {query}</h1>
      <div className="users-list">
        {users.map((item) => (
          <UserInfo key={item.id} login={item.login} avatar_url={item.avatar_url} />
        ))}
      </div>
    </>
  );
};
