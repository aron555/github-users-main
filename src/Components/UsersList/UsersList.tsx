import React, { FC, useEffect, useState } from 'react';
import './UsersList.css';
import { getUsers } from '../../api';
import { TUserData } from '../../types';
import { UserInfo } from '@components/UserInfo/UserInfo';
import { Loader } from '@components/Loader/Loader';

export const UsersList: FC = () => {
  const [users, setUsers] = useState<TUserData[]>([]);

  useEffect(() => {
    (async () => {
      setUsers(await getUsers());
    })();
  }, [users]);

  if (users.length === 0) {
    return <Loader />;
  }

  return (
    <div className="users-list">
      {users.map((item) => (
        <UserInfo key={item.id} login={item.login} avatar_url={item.avatar_url} />
      ))}
    </div>
  );
};
