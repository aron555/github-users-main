const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const EslintWebpackPlugin = require('eslint-webpack-plugin');
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');
const Dotenv = require('dotenv-webpack');

// Try the environment variable, otherwise use root
const ASSET_PATH = process.env.ASSET_PATH ? process.env.ASSET_PATH : '/';

module.exports = {
  mode: process.env.NODE_ENV || 'production',
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  entry: './src/index.tsx',
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
    alias: {
      '@components': path.resolve('./src/Components'),
    },
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.[contenthash].js',
    publicPath: ASSET_PATH,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        type: 'asset/resource',
      },
      {
        test: /\.(ts|tsx)$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
    new EslintWebpackPlugin({
      files: 'src/{**/*.*}.{tsx,ts,js}',
    }),
    new StylelintWebpackPlugin({
      files: 'src/{**/*.*}.css',
    }),
    new Dotenv({
      systemvars: true,
    }),
  ],
  devServer: {
    client: {
      overlay: false,
    },
    open: false,
    historyApiFallback: true,
  },
};
