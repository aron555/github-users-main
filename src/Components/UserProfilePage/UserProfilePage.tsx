import React, { FC, useEffect, useState } from 'react';
import './UserProfilePage.css';
import { useNavigate, useParams } from 'react-router-dom';
import { getRepo, getUser } from '../../api';
import { TReposData, TUserData } from '../../types';
import { UserRepoList } from '@components/UserRepoList/UserRepoList';
import { Loader } from '@components/Loader/Loader';

export const UserProfilePage: FC = () => {
  const { id }: { id?: string } = useParams();

  const navigate = useNavigate();

  if (id === undefined) {
    navigate('/');
    return null;
  }

  const [user, setUser] = useState<TUserData | null>(null);

  const [repo, setRepo] = useState<TReposData[] | null>(null);

  useEffect(() => {
    (async () => {
      setUser(await getUser(id));
      setRepo(await getRepo(id));
    })();
  }, [id]);

  if (user === null) {
    return <Loader />;
  }

  const followers = user.followers > 1000 ? (user.followers / 1000).toFixed(1) + 'k' : user.followers;

  return (
    <>
      <section className="user-profile">
        <div className="user-profile__image-container">
          <img className="user-profile__image" src={user.avatar_url} alt={`${user.login} profile photo`} />
        </div>
        <div className="user-profile__content">
          <h1 className="user-profile__title">
            {user.name}, <span className="user-profile__accent">{user.login}</span>
          </h1>
          <p className="user-profile__text">
            <span className="user-profile__accent">{followers}</span> followers ·{' '}
            <span className="user-profile__accent">{user.following}</span> following{' '}
            {user?.blog && (
              <a href={user.blog} className="link">
                {user.blog}
              </a>
            )}
          </p>
        </div>
      </section>

      <section className="repository-list">
        <div className="repository-list__header">
          <h2 className="repository-list__title">Репозитории</h2>
          <a
            href={`https://github.com/${user.login}?tab=repositories`}
            className="link"
            target="_blank"
            rel="noreferrer"
          >
            Все репозитории
          </a>
        </div>

        <div className="repository-list__container">
          {repo?.slice(0, 6).map((item) => (
            <UserRepoList key={item.id} item={item} />
          ))}
        </div>
      </section>
    </>
  );
};
